# WillowTree Name Game

To run, clone repository and run a server locally under *localhost*.
Running some kind of actual web server is going to cause the service worker to fail.

Running live at [namegame.garbee.me](https://namegame.garbee.me/) but, not with complete offline support.
You will also need to Allow Mixed content via your browser.
In Chrome, this is exposed via a shield icon in the right side of the Omnibar.
Images can't be cached by the service worker due to being served over an insecure connection.

## Contents

1. Name game using the default style of 5 choices to a name.
2. Scoreboard to show the number of times asked and the number correct, with percentage.
3. IndexedDB store of the people available.
4. Offline support via Service Worker so you can play on-the-go!
5. Web Components used for the scoreboard and people entries for selection.
6. React branch contains an alternative implementation using ReactJS (with some slight modifications to UX.)
