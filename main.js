if (navigator.serviceWorker) {
  navigator.serviceWorker.register('sw.js');
}
class Controller {
  constructor() {
    this._db = this.setupDb();
    this.currentPeople = [];
    this.lookingFor = 0;
    this.nameHolder = document.querySelector('span.name');
    this.peopleArea = document.querySelector('.people');
    if (!localStorage.getItem('askedAmount')) {
      localStorage.setItem('askedAmount', 0);
    }
    if (!localStorage.getItem('correctAmount')) {
      localStorage.setItem('correctAmount', 0);
    }
    this.createScoreboard();
    this.scoreboard = document.querySelector('willow-scoreboard');
    this.responseArea = document.querySelector('#response');
  }

  createScoreboard() {
    let element = document.createElement('willow-scoreboard');
    element.setAttribute('asked', localStorage.getItem('askedAmount'));
    element.setAttribute('correct', localStorage.getItem('correctAmount'));
    document.body.appendChild(element);
  }

  setupDb() {
    return idb.open('willowtree', 1, upgradeDb => {
      switch (upgradeDb.oldVersion) {
        case 0:
          var peopleStore = upgradeDb.createObjectStore('people',
          {keyPath: 'id', autoIncrement: true});
          peopleStore.createIndex('name', 'name');
          peopleStore.createIndex('url', 'url');
      }
    });
  }

  initDatabase() {
    if (localStorage.getItem('people-seeded') !== "true") {
      return fetch('https://namegame.garbee.me/people.json')
      .then(response => response.json())
      .then(data => {
        this._db.then(db => {
          let tx = db.transaction('people', 'readwrite');
          data.forEach(person => {
            tx.objectStore('people').add(person);
          });
          return tx.complete;
        });
        localStorage.setItem('people-seeded', true);
      });
    }
    return Promise.resolve("Database already initialized");
  }

  getRandom(list, amount = 5) {
    let stuff = [];
    for (let i = 0; i < amount; i++) {
      var entry = Math.floor((Math.random() * list.length));
      stuff.push(list[entry]);
      list.splice(entry, 1);
    }
    return stuff;
  }

  getRandomPeople(amount = 5) {
    if (amount <= 1) {
      throw new Error('Must ask for more than one person.');
    }
    return this._db.then(db => {
      let tx = db.transaction('people', 'readonly');
      tx.objectStore('people').getAllKeys().then(data => {
        let peopleKeys = this.getRandom(data, amount);
        peopleKeys.forEach(key => {
          tx.objectStore('people').get(key).then(data => {
            this.currentPeople.push(data);
          });
        });
      });
      return tx.complete;
    });
  }

  displayCurrentPeople() {
    this.currentPeople.forEach(person => {
      var element = document.createElement('willow-person');
      element.setAttribute('index', person.id);
      element.setAttribute('img', person.url);
      element.setAttribute('person-name', person.name);
      element.addEventListener('click', this.onPersonClick.bind(this));
      this.peopleArea.appendChild(element);
    });
  }

  clearCurrentPeople() {
    this.currentPeople = [];
    while (this.peopleArea.firstChild) {
      this.peopleArea.removeChild(this.peopleArea.firstChild);
    }
  }

  lookForFromCurrent() {
    let person = this.getRandom(this.currentPeople, 1)[0];
    this.lookingFor = person.id;
    this.nameHolder.textContent = person.name;
  }

  updateView() {
    this.responseArea.textContent = '';
    localStorage.setItem('askedAmount',
    (parseInt(localStorage.getItem('askedAmount')) + 1));
    this.updateScoreboard();
    this.clearCurrentPeople();
    this.getRandomPeople(5).then(data => {
      this.displayCurrentPeople();
      this.lookForFromCurrent();
    });
  }

  onPersonClick(event) {
    if (parseInt(event.target.getAttribute('index')) === this.lookingFor) {
      this.correctPersonSelected();
      return;
    }
    this.incorrectPersonSelected();
  }

  correctPersonSelected() {
    this.responseArea.textContent = 'Correct';
    localStorage.setItem('correctAmount',
    (parseInt(localStorage.getItem('correctAmount')) + 1));
    setTimeout(this.updateView.bind(this), 2000);
  }

  incorrectPersonSelected() {
    this.responseArea.textContent = 'Incorrect';
  }

  /**
   * Update the scoreboard view.
   */
  updateScoreboard() {
    this.scoreboard.setAttribute('asked', localStorage.getItem('askedAmount'));
    this.scoreboard.setAttribute('correct',
    localStorage.getItem('correctAmount'));
  }
}

// Startup the page.
let pageController = new Controller();
pageController.initDatabase().then(() => {
  pageController.updateView();
});
