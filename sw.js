let VERSION = 1;
let CACHE_NAME = 'willowtree-name-game-v';
let urlsToCache = [
  '/',
  '/style.css',
  '/main.js',
  '/third_party/idb.js',
  '/elements/scoreboard.html',
  '/elements/person.html',
];
self.addEventListener('install', function (event) {
  event.waitUntil(Promise.all([
    fetch('https://namegame.garbee.me/people.json')
    .then(response => response.json()).then(json => {
      let urls = [];
      json.forEach(item => {
        urls.push(item.url);
      });
      return urls;
    }).then(urls => {
      caches.open(CACHE_NAME + VERSION).then(cache => {
        cache.addAll(urls);
      })
    }),
    caches.open(CACHE_NAME + VERSION)
    .then(function (cache) {
      return cache.addAll(urlsToCache);
    })
  ]));
});
self.addEventListener('fetch', function (event) {
  event.respondWith(
  caches.match(event.request)
  .then(function (response) {
    if (response) {
      return response;
    }
    return fetch(event.request).then(response => {
      caches.open(CACHE_NAME + VERSION).then(cache => {
        cache.put(event.request, response.clone());
      });
      return response;
    });
  }
  )
  );
});
